#!/bin/bash
## Author: Michael Ramsey
## Objective Find A Reseller's/cPanel users accounts and all of their domains for cPanel.
## https://gitlab.com/cpaneltoolsnscripts/cpanel-find-reseller-domains
## How to use.
## ./find_reseller_domains.sh resellerusername
## How to use without downloading and running
##bash <(curl https://gitlab.com/cpaneltoolsnscripts/cpanel-find-reseller-domains/raw/master/find_reseller_domains.sh || wget -O - https://gitlab.com/cpaneltoolsnscripts/cpanel-find-reseller-domains/raw/master/find_reseller_domains.sh) resellerusername;

Reseller="$1"; readarray -t reseller_acct_array < <(sudo grep $Reseller /etc/trueuserowners|awk -F":" '{print $1}'| tr '/' '\n');echo ""; echo "Find $Reseller cPanel User's accounts"; echo ""; for ACCT in "${reseller_acct_array[@]}"; do echo $ACCT; done; readarray -t reseller_domains_array < <(echo ${reseller_acct_array[*]}| tr ' ' '\n' | sudo fgrep -f - /etc/userdomains | cut -d: -f1 |sort -u|rev | sort | awk 'NR!=1&&substr($0,0,length(p))==p{next}{p=$0".";print}'|rev|sort| tr '/' '\n');echo ""; echo "Find $Reseller cPanel User's Domains"; echo ""; for DOMAIN in "${reseller_domains_array[@]}"; do echo $DOMAIN; done;
echo ""
echo "Mission Completed"